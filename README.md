# Map scrapper

Receive an incomplete place information (e.g. address) and perform a search with map search engine (OSM, Google Maps) 
to collect missing information (e.g. coordinates, rating, etc.)

## Structure

`MapClientBase.py` -- defines the map search loop.

`MapClient*.py` -- defines source specific data procession algorithms

`MapSearchEngines.py` -- contains particular implementation of the search engine (OSM, Google maps, etc.)

`DbApi.py` -- defines communication with the database API to get search targets/store new info


## Run

```bash
python3 main.py --osm --google
```

Source of the address data should be provided with `--source` flag

```bash
python3 main.py --osm --source=dejup
python3 main.py --osm --source=edenred
```
