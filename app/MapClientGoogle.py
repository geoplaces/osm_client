"""Implementation for the Google map search."""

import logging
from datetime import datetime
from typing import Dict, List

from .DbApi import DbApiClient
from .MapClientBase import MapClientBase
from .MapSearchEngines import GoApiClient


class GoogleClient(MapClientBase):
    """Impl of search engine for Google."""

    def __init__(self, google_url, google_key, request_sleep, storage_url, api_client, api_passw, source, way=False):
        super().__init__(request_sleep, source)

        self.map_source = GoApiClient(google_url, google_key)
        self.storage = DbApiClient(storage_url, api_client, api_passw, source, 'google', way)

        self.logger = logging.getLogger('app.GoogleClient')
        self.logger.info('Google client init ok')

    @classmethod
    def filter_map_search_results(cls, map_places: List[Dict], zipcode: int) -> Dict:
        """Select one search result.

        Map provider specific!"""
        map_place = map_places[0]
        return map_place

    @classmethod
    def create_db_record(cls, map_place: Dict) -> Dict:
        """Cast map provider data to DB record.

        Map provider specific!"""

        return {
            'id': map_place['place_id'],
            'name': map_place.get('name'),
            'address': map_place['formatted_address'],
            'lat': float(map_place['geometry']['location']['lat']),
            'long': float(map_place['geometry']['location']['lng']),
            'updated_at': datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ'),
        }
