"""Implementation for the OSM map search."""

from typing import Dict, List

from datetime import datetime
import logging

from .MapClientBase import MapClientBase
from .MapSearchEngines import OsmApiClient
from .DbApi import DbApiClient


class OsmClient(MapClientBase):
    """Impl of search engine for OSM."""

    def __init__(self, osm_url, email, request_sleep, storage_url, api_client, api_passw, source):
        super().__init__(request_sleep, source)

        self.map_source = OsmApiClient(osm_url, email)
        self.storage = DbApiClient(storage_url, api_client, api_passw, source, 'osm', False)

        self.logger = logging.getLogger('app.OsmClient')
        self.logger.info('OSM client init ok')


    def filter_map_search_results(self, map_places: List[Dict], zipcode: int) -> Dict:
        """Select one search result.

        Map provider specific!"""
        map_place = {}

        for test_place in map_places:
            # if postcode is way different skip the place
            try:
                if test_place['address'].get('postcode') and int(test_place['address'].get('postcode')) != int(zipcode):
                    continue
            except ValueError:
                self.logger.warning(f"Wrong index received {test_place['address'].get('postcode')}")
                continue

            map_place = test_place
            # rely on "node" records rather than "way" and others
            if test_place['osm_type'] == 'node':
                break

        return map_place

    @classmethod
    def create_db_record(cls, map_place: Dict) -> Dict:
        """Cast map provider data to DB record.

        Map provider specific!"""

        city = map_place['address'].get('city')
        if not city:
            city = map_place['address'].get('town')
        if not city:
            city = map_place['address'].get('village')

        return {
            'id': str(map_place['osm_id']),
            'osm_type': map_place['osm_type'],
            'name': map_place.get('display_name'),
            'house_number': map_place['address'].get('house_number'),
            'road': map_place['address'].get('road'),
            'town': city,
            'postcode': map_place['address'].get('postcode'),
            'lat': float(map_place['lat']),
            'long': float(map_place['lon']),
            'updated_at': datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ'),
        }
