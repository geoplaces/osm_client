"""Map search engines implementation.

E.g. calls to OSM/Google API with location search.
"""
import logging
import urllib.parse
from abc import abstractmethod, ABC
from time import sleep
from typing import Dict, List

import requests
from urllib3.exceptions import NewConnectionError


class SearchEngine(ABC):
    def __init__(self):
        ...

    @abstractmethod
    def map_search_request(self, city: str, street: str, postalcode: str) -> List[Dict]:
        ...


class OsmApiClient(SearchEngine):
    """OSM search engine call."""

    def __init__(self, osm_url: str, email: str):
        super().__init__()
        self.osm_url = osm_url + '/search'
        self.email = email
        self.logger = logging.getLogger('app.OsmApiClient')

    def map_search_request(self, city: str, street: str, postalcode: str) -> List[Dict]:
        """Make a search with OSM API."""
        search_params = {'city': city,
                         'street': street,
                         'postalcode': postalcode,
                         'format': 'json',
                         'addressdetails': 1,
                         'namedetails': 1,
                         'email': self.email
                         }
        encoded = urllib.parse.urlencode(search_params)
        headers = {'User-Agent': 'PostmanRuntime/7.36.1'}
        try:
            resp = requests.get(self.osm_url, params=encoded, headers=headers)
        except NewConnectionError as e:
            self.logger.warning(f'Failed to connect. Sleep 120 and try again: {e}')
            sleep(120)
            resp = requests.get(self.osm_url, params=encoded, headers=headers)

        self.logger.debug(f'Asked OSM with {resp.url}')

        if resp.status_code >= 500:
            self.logger.warning(f'OSM returns {resp.status_code}. Wait for 120 s and retry')
            sleep(120)
            resp = requests.get(self.osm_url, params=encoded)

        if resp.status_code != 200:
            self.logger.error(
                f'OSM request {search_params} failed with status {resp.status_code} and body `{resp.text}`')
            return []

        return resp.json()


class GoApiClient(SearchEngine):
    """Google map search engine call."""

    def __init__(self, google_url, google_key):
        super().__init__()
        self.url = google_url
        self.key = google_key

        self.logger = logging.getLogger('app.GoApiClient')

    def map_search_request(self, city: str, street: str, postalcode: str) -> List[Dict]:
        search_params = {
            'key': self.key,
            'fields': 'formatted_address,name,geometry,place_id',
            'inputtype': 'textquery',
            'input': f'{street} {postalcode} {city}'
        }
        encoded = urllib.parse.urlencode(search_params)
        resp = requests.get(self.url + '/json', params=encoded)
        self.logger.debug(f'Asking Google maps with {resp.url}')
        if resp.status_code != 200:
            self.logger.exception(
                f'Bad Google maps response, status {resp.status_code} and body {resp.text}')

        return resp.json()['candidates']
