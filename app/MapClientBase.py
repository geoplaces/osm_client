"""
Abstract class to update places.

1. Ask DB API for places to be updated
2. Search with some map search engine for locations
3. Store found locations in the DB API

Children should overload filter_map_search_results() and create_db_record()
"""

import re
from abc import ABC, abstractmethod
from time import sleep
from typing import Dict, List

from app.DbApi import DbApiClient
from app.MapSearchEngines import SearchEngine


class MapClientBase(ABC):
    """Base abstract class for map lookup."""

    def __init__(self, request_sleep, source: str):
        # request frequency
        self.request_sleep = request_sleep
        # Map search engine
        self.map_source: SearchEngine | None = None
        # Interaction with DB API
        self.storage: DbApiClient | None = None

        self.source = source

        self.logger = None

        self.errors = []

    def initialise(self) -> bool:
        if self.storage is None:
            return False
        return self.storage.initialise()

    def get_place_with_address(self, city: str, street: str, postalcode: str) -> List[Dict]:
        """Prepare the request to map provider."""
        resp = self.map_source.map_search_request(city, street, postalcode)

        if len(resp) == 0:
            # if nothing is found try with a street only, omit house number
            street = re.sub('^\d*\s', '', street)
            sleep(self.request_sleep)
            return self.map_source.map_search_request(city, street, postalcode)

        return resp

    def update_place(self, place: Dict, map_place: Dict):
        if len(map_place) == 0:
            self.logger.debug('Empty map search, add dummy')
            self.link_dummy(place['associate_id'])
        else:
            record = self.create_db_record(map_place)
            if self.store_map_places(record):
                self.link_map_place(self.source, place['associate_id'], record["id"])

    def store_map_places(self, data: Dict) -> bool:
        """Store the place in DB."""
        self.logger.debug('Store place')
        if not self.storage.store_places_request(data):
            self.errors.append(f'Failed to store place {data}')
            return False
        self.logger.debug(f'Store place {data["id"]} ok')

        return True

    def link_map_place(self, source: str, associate_id: int, place_id: str):
        self.logger.debug('Link place')
        if not self.storage.link_places_request(source, associate_id, place_id):
            self.errors.append(f'Failed to link place with id {place_id}')
        self.logger.debug(f'Link place {place_id} ok')

    def link_dummy(self, associate_id: int):
        if not self.storage.put_dummy_request(associate_id):
            self.errors.append(f'Failed to link dummy for id {associate_id}')

    @classmethod
    @abstractmethod
    def filter_map_search_results(cls, map_places: List[Dict], zipcode: int) -> Dict:
        """Select one search result.

        Map provider specific!"""
        ...

    @classmethod
    @abstractmethod
    def create_db_record(cls, map_place: Dict) -> Dict:
        """Cast map provider data to DB record.

        Map provider specific!"""

        ...

    def add_map_places(self, **kwargs):
        """Main loop for adding place into DB."""

        self.logger.info('Start map places adding')
        places_to_update = self.storage.get_place_no_map_request()

        processed = 0
        while len(places_to_update) > 0:
            for place in places_to_update:
                sleep(self.request_sleep)
                map_places = self.get_place_with_address(place['city'], place['address'], place['zipcode'])
                self.logger.debug(f'Map search with `{len(map_places)}` results')

                map_place = {}
                if len(map_places) > 0:
                    map_place = self.filter_map_search_results(map_places, int(place['zipcode']))

                self.update_place(place, map_place)

                processed += 1
                if kwargs.get('limit') is not None and processed >= kwargs['limit']:
                    break

            if kwargs.get('limit') is not None and processed >= kwargs['limit']:
                break
            # make a new request
            last_id = places_to_update[-1]['associate_id']
            self.logger.info(f'Next request from id `{last_id}`')
            places_to_update = self.storage.get_place_no_map_request(last_id)

        self.logger.info('Map places adding ok')
