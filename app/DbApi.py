import json
import logging
from abc import ABC
from datetime import datetime, timedelta
from typing import List, Dict

import requests


class DbApiClient(ABC):
    """Abstract client to access DB API.

    Children should define own endpoint URLs.
    """

    def __init__(self, url: str, api_client: str, api_passw: str, source: str, target: str, way: bool = False):
        self.url = url
        self.user = api_client
        self.passw = api_passw
        self.logger = logging.getLogger('app.DbApiOsmClient')

        self.token = ''
        self.token_exp = 0

        self.version = '/v1'

        self.path_dummy = f'/associations/{source}'
        self.path_dummy_postfix = f'/dummy/{target}'
        self.path_post = f'/places/{target}/batch'
        self.path_get_for_search = f'/associations/{source}?query='
        self.path_get_for_search += 'no_osm' if target == 'osm' else 'osm_dummy_no_google'
        self.associate_name = 'osm_id' if target == 'osm' else 'gomaps_id'
        if way:
            self.path_get_for_search = f'/associations/{source}?query=osm_way'

    def initialise(self) -> bool:
        """Request token based on client creds."""
        return self.get_token() != ''

    def get_header(self) -> Dict[str, str]:
        return {'Content-Type': 'application/json',
                'Authorization': f'Bearer {self.get_token()}'}

    def get_token(self) -> str:
        if self.token != '' and self.token_exp > (datetime.now() + timedelta(seconds=10)).timestamp():
            return self.token
        body = {
            'scope': 'read:associate write:associate write:osm write:google',
            'username': self.user,
            'password': self.passw
        }
        resp = requests.post(f'{self.url}{self.version}/auth/token', data=body)
        if resp.status_code != 200:
            return ''

        self.token = resp.json().get('access_token')
        self.token_exp = resp.json().get('exp')
        return self.token

    def put_dummy_request(self, associate_id: int) -> bool:
        """Add a dummy link to associate_id record to mark a failed search."""
        resp = requests.post(
            f'{self.url}{self.version}{self.path_dummy}/{associate_id}' + self.path_dummy_postfix,
            headers=self.get_header())
        if resp.status_code != 200:
            self.logger.exception(f'DB response has status {resp.status_code}')
            return False

        return True

    def store_places_request(self, data: Dict) -> bool:
        """Add place with post."""
        resp = requests.post(f'{self.url}{self.version}{self.path_post}',
                             headers=self.get_header(), data=json.dumps([data]))
        if resp.status_code != 201:
            self.logger.exception(
                f'DB storage response has status {resp.status_code}')
            return False

        return True

    def link_places_request(self, source: str, associate_id: int, place_id: str) -> bool:
        data = {self.associate_name: place_id}
        resp = requests.patch(f'{self.url}{self.version}/associations/{source}/{associate_id}',
                              headers=self.get_header(),
                              data=json.dumps(data))
        if resp.status_code != 200:
            self.logger.exception(
                f'DB storage link response has status {resp.status_code} and text {resp.text}')
            return False

        return True

    def get_place_no_map_request(self, start: int = -1) -> List[Dict]:
        """GET source places with no proper map links."""
        params = {}
        if start > 0:
            params['start_id'] = start
        resp = requests.get(
            f'{self.url}{self.version}{self.path_get_for_search}', headers=self.get_header(), params=params)
        if resp.status_code != 200:
            self.logger.exception(
                f'DB storage response has status {resp.status_code}')
            return []

        return resp.json()
