import logging.config
from os import getenv as ge

import click
import requests
import sentry_sdk
import yaml
from dotenv import load_dotenv

from app.MapClientBase import MapClientBase
from app.MapClientGoogle import GoogleClient
from app.MapClientOsm import OsmClient

load_dotenv()


def get_osm_client(source: str) -> MapClientBase:
    osm_url = ge('OSM_URL')
    osm_email = ge('OSM_EMAIL')
    request_sleep = int(ge('OSM_REQUEST_SLEEP'))

    storage_url = ge('URL')
    api_client = ge('API_CLIENT')
    api_passw = ge('API_PASSW')

    client = OsmClient(osm_url, osm_email, request_sleep, storage_url, api_client, api_passw, source)

    return client


def get_google_client(target: str, way: bool = False) -> MapClientBase:
    google_url = ge('GOOGLE_URL')
    google_key = ge('GOOGLE_KEY')
    request_sleep = float(ge('GOOGLE_REQUEST_SLEEP'))

    storage_url = ge('URL')
    api_client = ge('API_CLIENT')
    api_passw = ge('API_PASSW')

    client = GoogleClient(google_url, google_key, request_sleep, storage_url, api_client, api_passw, target, way)

    return client


def run(client: MapClientBase, **kwargs):
    if client is None:
        raise RuntimeError('Null map client')
    if not client.initialise():
        raise RuntimeError('Fail to initialise API client')
    client.add_map_places(**kwargs)


@click.command()
@click.option('--osm', is_flag=True, help='Run OSM engine')
@click.option('--google', is_flag=True, help='Run Google maps engine')
@click.option('--way', is_flag=True, help='Update OSM way records as well')
@click.option('--source', '-s', type=str, default="dejup")
@click.option('--no_log', '-l', is_flag=True, help="Do not read log config", default=False)
@click.option('--limit', '-n', type=int, help="Limit number of places to update", default=-1)
def main(osm, google, way, source, no_log, limit):
    dsn = ge('SENTRY_DSN')
    if dsn is not None and dsn != '':
        sentry_sdk.init(
            dsn=dsn,
            # Set traces_sample_rate to 1.0 to capture 100%
            # of transactions for performance monitoring.
            traces_sample_rate=1.0,
            # Set profiles_sample_rate to 1.0 to profile 100%
            # of sampled transactions.
            # We recommend adjusting this value in production.
            profiles_sample_rate=1.0,
        )

    if not no_log:
        with open("logging.yml", "r") as f:
            yaml_config = yaml.safe_load(f.read())
            logging.config.dictConfig(yaml_config)

    logger = logging.getLogger('app.main')
    try:
        client = None
        if osm:
            client = get_osm_client(source)

        elif google:
            client = get_google_client(source, way)
        kwargs = {}
        if limit > 0:
            kwargs = {'limit': limit}

        run(client, **kwargs)

        heart_beat_url = ge('HEART_BEAT_URL')
        if len(client.errors) == 0 and heart_beat_url is not None and heart_beat_url != '':
            resp = requests.get(heart_beat_url)
            if resp.status_code != 200:
                logger.warning(f'Heart beat failed with {resp.status_code}')
    except:
        logger.exception('Error during map search.')


if __name__ == "__main__":
    main()
