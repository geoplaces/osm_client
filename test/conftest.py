from datetime import datetime, timedelta
from os import getenv as ge

import pytest
import requests_mock
from dotenv import load_dotenv

load_dotenv()
URL = ge('URL')


@pytest.fixture
def mock_api_wo_token():
    with requests_mock.Mocker(real_http=True) as m:
        yield m


@pytest.fixture
def mock_api(mock_api_wo_token):
    mock_api_wo_token.post(f'{URL}/v1/auth/token', status_code=200,
                           json={'access_token': 'some_token',
                                 'exp': (datetime.now() + timedelta(hours=1)).timestamp()})

    yield mock_api_wo_token
