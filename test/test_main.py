from datetime import datetime, timedelta
from os import getenv as ge

from click.testing import CliRunner
from dotenv import load_dotenv

from main import main
from test.test_google_client import SAMPLE_NO_GOOGLE
from test.test_osm_client import SAMPLE_NO_OSM

load_dotenv()
URL = ge('URL')


def test_main_osm(mock_api):
    mock_api.get(f'{URL}/v1/associations/dejup?query=no_osm', json=SAMPLE_NO_OSM, status_code=200)
    mock_api.get(f'{URL}/v1/associations/dejup?query=no_osm&start_id=15020', json={}, status_code=200)
    mock_api.post(f'{URL}/v1/places/osm/batch', status_code=201)
    mock_api.patch(f'{URL}/v1/associations/dejup/15020', status_code=200)

    runner = CliRunner()
    result = runner.invoke(main, ["--osm", "--no_log"])

    assert result.exit_code == 0
    assert mock_api.call_count > 5


def test_main_google(mock_api):
    mock_api.get(f'{URL}/v1/associations/dejup?query=osm_dummy_no_google', json=SAMPLE_NO_GOOGLE, status_code=200)
    mock_api.get(f'{URL}/v1/associations/dejup?query=osm_dummy_no_google&start_id=15020', json={}, status_code=200)
    mock_api.post(f'{URL}/v1/associations/dejup/15020/dummy/google', status_code=200)

    runner = CliRunner()
    result = runner.invoke(main, ["--google", "--no_log"])

    assert result.exit_code == 0
    assert mock_api.call_count > 5
