from copy import deepcopy
from datetime import datetime, timedelta
from os import getenv as ge

from dotenv import load_dotenv

from app.MapClientGoogle import GoogleClient
from main import get_google_client

load_dotenv()
URL = ge('URL')

SAMPLE_GOOGLE = {
    "formatted_address": "74 Rue Rambuteau, 75001 Paris, France",
    "geometry": {
        "location": {
            "lat": 48.8621737,
            "lng": 2.3496662
        },
        "viewport": {
            "northeast": {
                "lat": 48.86343302989272,
                "lng": 2.351259229892722
            },
            "southwest": {
                "lat": 48.86073337010728,
                "lng": 2.348559570107278
            }
        }
    },
    "name": "74 Rue Rambuteau",
    "place_id": "ChIJo8KdQxlu5kcR_NVhlo2TU34"
}


def test_filter():
    places = [deepcopy(SAMPLE_GOOGLE)]
    place = GoogleClient.filter_map_search_results(places, 1)
    assert place.get('name')


def test_create_record():
    place = deepcopy(SAMPLE_GOOGLE)
    record = GoogleClient.create_db_record(place)
    assert record.get('address')


SAMPLE_NO_GOOGLE = [{
    "address": "11 RUE JOSEPH DE MAISTRE",
    "zipcode": 75018,
    "city": "PARIS",
    "associate_id": 15020
}]


def test_google_search(mock_api):
    mock_api.get(f'{URL}/v1/associations/dejup?query=osm_dummy_no_google', json=SAMPLE_NO_GOOGLE, status_code=200)
    mock_api.get(f'{URL}/v1/associations/dejup?query=osm_dummy_no_google&start_id=15020', json={}, status_code=200)
    mock_api.post(f'{URL}/v1/associations/dejup/15020/dummy/google', status_code=200)

    google_client = get_google_client('dejup')
    google_client.add_map_places()

    assert mock_api.call_count == 6
