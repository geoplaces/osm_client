from copy import deepcopy
from os import getenv as ge

from dotenv import load_dotenv

from app.MapClientOsm import OsmClient
from main import get_osm_client

load_dotenv()
URL = ge('URL')

SAMPLE_NO_OSM = [{
    "address": "11 RUE JOSEPH DE MAISTRE",
    "zipcode": 75018,
    "city": "PARIS",
    "associate_id": 15020
}]

SAMPLE_ERROR_OSM = [{
    "address": "wrong",
    "zipcode": 75018,
    "city": "PARIS",
    "associate_id": 15020
}]


def test_filter():
    places = [deepcopy(SAMPLE_OSM)]
    osm_client = get_osm_client('dejup')
    place = osm_client.filter_map_search_results(
        places, int(SAMPLE_OSM['address']['postcode'] + '1'))
    assert len(place) == 0

    places = [deepcopy(SAMPLE_OSM), deepcopy(SAMPLE_OSM)]
    places[0]['osm_type'] = 'way'
    places[1]['name'] = 'rest'
    place = osm_client.filter_map_search_results(
        places, int(SAMPLE_OSM['address']['postcode']))
    assert place['name'] == 'rest'


def test_create_record():
    place = deepcopy(SAMPLE_OSM)
    record = OsmClient.create_db_record(place)
    assert record.get('town')

    place['address'].pop('city')
    place['address']['town'] = 'Paris'
    record = OsmClient.create_db_record(place)
    assert record.get('town')

    place['address'].pop('town')
    place['address']['village'] = 'Paris'
    record = OsmClient.create_db_record(place)
    assert record.get('town')


def test_failed_init(mock_api_wo_token):
    mock_api_wo_token.post(f'{URL}/v1/auth/token', status_code=401)
    osm_client = get_osm_client('dejup')
    assert not osm_client.initialise()


def test_osm_prepare_failed(mock_api):
    mock_api.get(f'{URL}/v1/associations/dejup?query=no_osm', status_code=401)

    osm_client = get_osm_client('dejup')
    osm_client.add_map_places()
    assert mock_api.call_count == 2
    assert len(osm_client.errors) == 0

def test_osm_search(mock_api):
    mock_api.get(f'{URL}/v1/associations/dejup?query=no_osm', json=SAMPLE_NO_OSM, status_code=200)
    mock_api.get(f'{URL}/v1/associations/dejup?query=no_osm&start_id=15020', json={}, status_code=200)
    mock_api.post(f'{URL}/v1/places/osm/batch', status_code=201)
    mock_api.patch(f'{URL}/v1/associations/dejup/15020', status_code=200)

    osm_client = get_osm_client('dejup')
    osm_client.add_map_places()
    assert mock_api.call_count == 6
    assert len(osm_client.errors) == 0

def test_osm_batch_failed(mock_api):
    mock_api.get(f'{URL}/v1/associations/dejup?query=no_osm', json=SAMPLE_NO_OSM, status_code=200)
    mock_api.get(f'{URL}/v1/associations/dejup?query=no_osm&start_id=15020', json={}, status_code=200)
    mock_api.post(f'{URL}/v1/places/osm/batch', status_code=401)
    mock_api.patch(f'{URL}/v1/associations/dejup/15020', status_code=200)

    osm_client = get_osm_client('dejup')
    osm_client.add_map_places()
    assert mock_api.call_count == 5
    assert f'{URL}/v1/associations/dejup/15020' not in [req.url for req in mock_api.request_history]
    assert len(osm_client.errors) == 1

def test_osm_link_failed(mock_api):
    mock_api.get(f'{URL}/v1/associations/dejup?query=no_osm', json=SAMPLE_NO_OSM, status_code=200)
    mock_api.get(f'{URL}/v1/associations/dejup?query=no_osm&start_id=15020', json={}, status_code=200)
    mock_api.post(f'{URL}/v1/places/osm/batch', status_code=200)
    mock_api.patch(f'{URL}/v1/associations/dejup/15020', status_code=401)

    osm_client = get_osm_client('dejup')
    osm_client.add_map_places()
    assert mock_api.call_count == 5
    assert len(osm_client.errors) == 1


def test_add_dummy(mock_api):
    mock_api.get(f'{URL}/v1/associations/dejup?query=no_osm', json=SAMPLE_ERROR_OSM, status_code=200)
    mock_api.post(f'{URL}/v1/associations/dejup/15020/dummy/osm', status_code=200)
    mock_api.get(f'{URL}/v1/associations/dejup?query=no_osm&start_id=15020', json={}, status_code=200)

    osm_client = get_osm_client('dejup')
    osm_client.add_map_places()

    assert mock_api.call_count == 6
    assert f'{URL}/v1/associations/dejup/15020/dummy/osm' in [req.url for req in mock_api.request_history]
    assert len(osm_client.errors) == 0

def test_add_dummy_failed(mock_api):
    mock_api.get(f'{URL}/v1/associations/dejup?query=no_osm', json=SAMPLE_ERROR_OSM, status_code=200)
    mock_api.post(f'{URL}/v1/associations/dejup/15020/dummy/osm', status_code=401)
    mock_api.get(f'{URL}/v1/associations/dejup?query=no_osm&start_id=15020', json={}, status_code=200)

    osm_client = get_osm_client('dejup')
    osm_client.add_map_places()

    assert mock_api.call_count == 6
    assert f'{URL}/v1/associations/dejup/15020/dummy/osm' in  [req.url for req in mock_api.request_history]
    assert len(osm_client.errors) == 1


SAMPLE_OSM = {
    "name": "test",
    "osm_type": "node",
    "osm_id": "1",
    'lat': 0,
    'lon': 0,
    "address": {
        "city": "Paris",
        "postcode": "12345"
    }
}
