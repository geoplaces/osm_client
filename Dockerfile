FROM python:3.10.6-slim-buster

WORKDIR /usr/src/app
RUN mkdir -p /var/log/GeoPlaces

COPY . /usr/src/app/
COPY .env_example /usr/src/app/.env

RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install -r requirements.txt

CMD python3 main.py --osm
